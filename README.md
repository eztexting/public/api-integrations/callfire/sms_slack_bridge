# CallFire SMS to Slack bridge

Prepare your server
- your CallFire SMS to slack bridge is a web API application written in python; you need:
    - the application itself, that you can download from https://github.com/CallFire/sms_slack_bridge
    - a web server that can run python applications (e.g. nginx and uwsgi)
    - the following python libraries: bottle, requests
    - a dedicated domain name
    - ssl certificates for your domain name, properly configured in your web server settings
    - once everything is in place, try hitting this page from your browser to make sure it works: https://yourdomain.com/cf_slack_bridge_ping
- create a developer account with CallFire
    - go to the https://developers.callfire.com/docs.html#authentication webpage 
    - you will find your developer username and password; use them to set the values of the cf_login and cf_password variables in your app
    - you also need to set the CallFire phone number that you enable Slack for: enter it as a value for the variable cf_did (start with 1 then the 10 digits, no formatting characters please)
    - once everything is in place, try hitting this other page from your browser: https://yourdomain.com/cf_slack_bridge_settings
    - dont worry about the variables that still say "SET_ME", we will take care of them on the next few steps

Prepare Slack
- you need to have a Slack account and a team already created; in this case, your home slack page is https://(your_team).slack.com
- make sure you are logged in to Slack
- navigate to https://(your_team).slack.com/apps/build/custom-integration (home page -> Developers -> Get started -> Make a Custom Integration)
- create an "incoming webhook", to post text messages (SMS) received on your CallFire DID into a slack channel
    - on the first step, select the channel name (e.g. #lalala)
    - on the 2nd step, copy the webhook URL, and paste it in the application as a value for the variable slack_send_message_url
    - use a name and a descriptive label for your webhook (e.g. "receive_callfire_sms", and "CallFire SMS bridge - SMS to slack")
    - add an icon that will be displayed with every message (we would love if you would use this: -- callfire icon --)
- create an "outgoing webhook" 
    - choose the channel that would be the source of the text messages (e.g. #test), should be the same channel that you chose for the previous hook
    - set the trigger word to ^sms+1
    - set the URL that your website responds to, according to your installation; e.g. https://yourdomain.com/from_slack
    - change the value of the variable slack_hook_token to be the token that you see on the webhook page
    - enter values for the webhook name and descriptive label (e.g. "send_callfire_sms", and "CallFire SMS bridge - slack to SMS")
    - add an icon that will be displayed with every message (we would love if you would use this: -- callfire icon --)
- in your code, set the value of the variable slack_channel to the slack channel name you used (including its # prefix)

That's it. Now test your app by sending a text message from any mobile phone to your DID, it should show up in your slack channel. 
To send text messages from slack to a phone, prefix your message with the string ^sms+1 and the 10-digit phone number, like this: "^sms+18778973473 Hello world".
