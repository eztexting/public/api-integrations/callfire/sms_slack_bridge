import sys
import time
import json
import requests
import bottle

my_url = "SET ME"
cf_login = "SET ME"
cf_password = "SET ME"
cf_did = "SET ME"

## slack URL used to post messages to your channel, security token, and channel name
slack_send_message_url = "SET ME"
slack_hook_token = "SET ME"
slack_channel = "SET ME"


## detect the campaign associated with the DID number (if any)
cf_campaign = 0
rp = requests.get("https://api.callfire.com/v2/texts/broadcasts",
    auth=(cf_login, cf_password),
    params={ 'label': "slack_" + cf_did }
)
if rp.status_code in [200, 201]:
    campaign_json = rp.json()
    if campaign_json.get('totalCount', 0) > 0:
        cf_campaign = campaign_json['items'][0]['id']
        print("Using slack SMS campaign " + str(cf_campaign))
if cf_campaign == 0:
    print("No slack SMS campaign found, hopefully someone will initialize it soon")


@bottle.get("/cf_slack_bridge_ping")
def ping():
    return "<html><body>This is the CallFire SMS to Slack bridge running on " + my_url + ". Enjoy!</body></html>"

@bottle.get("/cf_slack_bridge_settings")
def settings():
    ## we need to create a campaign for our DID (if not already created)
    global cf_campaign
    if cf_campaign == 0:
        ## no campaign defined yet - create it now
        rp = requests.post("https://api.callfire.com/v2/texts/broadcasts",
            auth=(cf_login, cf_password),
            json={
                'name': "Text messages sent to slack from " + cf_did,
                'labels': [ "slack_" + cf_did ],
                'fromNumber': cf_did,
                'recipients': [],
            }
        )
        if rp.status_code in [200, 201]:
            campaign_json = rp.json()
            cf_campaign = campaign_json['id']
        else:
            return "Failed to create a text broadcast campaign, API response was " + str(rp.status_code)

    ## register a hook (if not already registered), that will trigger a Callfire API event 
    ## whenever we receive a text on our DID
    hook = 0
    rp = requests.get("https://api.callfire.com/v2/webhooks", 
        auth=(cf_login, cf_password),
        params={ 
           'callback': my_url + "/to_slack",
        }
    )
    if rp.status_code in [200, 201]:
        hooks = rp.json()
        for hook in hooks.get('items', []):
            if hook['name'] == "slack bridge for " + cf_did:
                hook = hook['id']
                print("Slack SMS hook already exists, id is " + str(hook))
                break
    if hook == 0:
        rp = requests.post("https://api.callfire.com/v2/webhooks",
            auth=(cf_login, cf_password),
            json={
                'name': "slack bridge for " + cf_did,
                'resource': "InboundText",
                'events': ['finished'],
                'callback': my_url + "/to_slack"
            }
        )
        if rp.status_code in [200, 201]:
            hook_json = rp.json()
            hook = hook_json['id']
            print("Subscribed for receiving events on inbound text (SMS) messages on " + cf_did + ", id is " + str(hook))
        else:
            return "Failed to create a hook for the incoming SMS, API response was " + str(rp.status_code)

    r = "<html><body>"
    r += "<strong>CallFire Slack bridge</strong> running on " + my_url + "<br/><br/>"
    r += "Successfully configured for the CallFire API, webhook ID is " + str(hook) + "<br/>"
    missing_vars = []
    if not slack_send_message_url.startswith("https://hooks.slack.com/services/"):
        missing_vars.append("slack_send_message_url")
    if slack_hook_token == "SET ME" or slack_hook_token == "":
        missing_vars.append("slack_hook_token")
    if not slack_channel.startswith("#"):
        missing_vars.append("slack_channel")
    if len(missing_vars):
        r += "You still need to configure the following variables: " + str(missing_vars) + "<br/>"
    else:
        r += "All variables for the Slack interface are set.<br/>"
    r += "</body></html>"
    return r


## receive text (SMS) message from CF API, pass it on as slack message
@bottle.post("/to_slack")
def sms_to_slack():
    print("SMS indication received: " + bottle.request.body.read())
    ## pick up the POST data - it's json
    rq = bottle.request.json
    for event in rq.get('events', []):
        if not event['resource']['inbound']:
            continue
        did = event['resource']['toNumber']
        sender = event['resource']['fromNumber']
        msg = event['resource']['message']
        media = event['resource']['media']   # this is an array

        rp = requests.post(
            slack_send_message_url,
            json={
                'channel': slack_channel,
                'attachments': [
                    {
                        'fallback': "*<@sms+" + sender + ">* says: " + msg,
                        'author_name': "sms+" + sender,
                        'text': msg,
                    },
                ]
            }
        )


## receive slack message from their API, pass it on as SMS
@bottle.post("/from_slack")
def slack_to_sms():
    content = bottle.request.body.read()
    print("slack indication received: " + str(content))

    if bottle.request.forms.get("token") != slack_hook_token:
        print("Invalid token, not my kind of message")
        return
    if bottle.request.forms.get("channel_name") != slack_channel[1:]:
        print("Invalid channel name, not what i was listening to")
        return

    message = bottle.request.forms.get("text")
    if message is not None:
        if not message.startswith("^sms+"):
            print("Stray message received, not what i would sms")
            return
        message_parts = message.split(" ", 1)
        dest = message_parts[0][5:]
        msg = message_parts[1]

        print("sending message '" + msg + "' to " + dest)
        rp = requests.post(
            "https://api.callfire.com/v2/texts?campaignId=" + str(cf_campaign),
            auth=(cf_login, cf_password),
            json=[{ 'phoneNumber': dest, 'message': msg, }]
        )


if __name__ == '__main__':
    bottle.run(host="localhost", port=8080)
else:
    app = application = bottle.default_app()

